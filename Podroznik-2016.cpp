//#include "stdafx.h"

#include <iostream>
#include <time.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>


using namespace std;

void game(int choice_bot1, int choice_bot2, int *w);
int bot_low(int points_bot1, int points_bot2, int round, int wybory1[], int wybory2[]);
int bot_high(int points_bot1, int points_bot2, int round, int wybory1[], int wybory2[]);

const int R = 25;
const int ilosc_gier = 1000;
int wybory1 [ilosc_gier];
int wybory2 [ilosc_gier];
 
int main()
{
    int j=0;
    int choice1 = 0, choice2 = 0;
    int SCORE[2] = {0, 0};
    int *s;
    s = SCORE;

    srand(time(NULL));
    do{
        cout << "Runda " << j+1 << endl;
        choice1 = bot_low(SCORE[0], SCORE[1], j, wybory1, wybory2);
        choice2 = bot_high(SCORE[1], SCORE[0], j, wybory2, wybory1);
        cout << "Gracz 1 wybral: " << choice1 << "\t";
        cout << "Gracz 2 wybral: " << choice2 << "\t" << endl;
        wybory1[j] = choice1;
        wybory2[j] = choice2;
        game(choice1, choice2, s);
        cout << endl << "Punktacja: \n\t" << SCORE[0] << "\n\t" << SCORE[1] << endl << endl << endl;
        j++;
    }
    while(j<ilosc_gier);
    cout << "Ncisnij ENTER aby kontynuowac...";
    getchar();
    system("CLS");
    cout << "Koniec gry!\n\t Gracz 1 uzyskal: "  << SCORE[0] << "\n\t Gracz 2 uzyskal: " 
    << SCORE[1] << endl;
    getchar();
    return 0;
}

void game(int choice_bot1, int choice_bot2, int *w)
{
    bool flag=0;
    if((choice_bot1 < 180) || (choice_bot1 > 300)){
        if((choice_bot2 >= 180) && (choice_bot2 <= 300))
            w[1] += choice_bot2 + R;
        flag = 1;
    }
    if(!flag && ((choice_bot2 < 180) || (choice_bot2 > 300))){
        if((choice_bot1 >= 180) && (choice_bot1 <= 300))
            w[0] += choice_bot1 + R;
        flag = 1;
    }
   
    if (!flag) {
    if(choice_bot1 == choice_bot2){
           w[0] += choice_bot1;
        w[1] += choice_bot1;
    }
    else{
        if(choice_bot1 > choice_bot2){
            w[0] += choice_bot2 - R;
            w[1] += choice_bot2 + R;
        }           
        else{
            w[0] += choice_bot1 + R;
            w[1] += choice_bot1 - R;
        }           
    }
    }
}


int bot_low(int points_bot1, int points_bot2, int round, int wybory1[], int wybory2[])
{   
    // s10120 - do kontaktu w sprawie bota
    
    // points_bot1 - suma punktów danego bota
    // points_bot2 - suma punktów drugiego bota
    // round - numer rundy
    // wybory1 - lista moich wyborów
    // wybory2 - lista wyborów przeciwnika

    /**
    Strategia:

    Początkowy etap gry dajemy wybór randomowy od 250 - 300 przez około 5 ruchów. - done!
    
    Sprawdzamy ostatnie 5 ruchów przeciwnika, wyliczamy średnią rónicę pomiędzy 
    wyborami przeciwnika następnie pomniejszamy wybór o tą średnią oraz 
    współczynnik zmiany (jaki to do ustalenia)

    co jakiś ruch podbijamy wybór do 300 - np dzielenie modulo (rundy + 1) 
    przez 10 oraz sprawdzenie wartości modulo z randomem z zakresu (0 - 3) 
    jeśli są równe to wtedy podbijamy wybór do 300 - 
    random zakresu wybieramy ponownie po podbiciu do 300
    */
    int result = 180;

    if (round < 6) {
        cout << "runda mniejsza od 6 \n";
        result = (rand() % 50) + 250;
    } else {
        cout << "runda powyzej 6 \n";
        if (round % 10 == 0) {
            cout << "module przez 10 jest rowne 0 \n";
            result = (rand() % 50) + 250;
        } else {
            int downsize = 0;
            int prev_choice = 0;
            cout << "poczatkowy downsize: " << downsize << "\n";
            for (int i = 1; i < 6; i++){
                int choice_value = wybory2[round - i];

                cout << "wybor z rundy: " << round - i << "\n";

                cout << "wybor 2 gracza: " << choice_value << "\n";
                cout << "poprzedni wybor 2 gracza: " << prev_choice << "\n";

                // wylicz o ile obnizamy
                if ( prev_choice != 0) {
                    downsize += (choice_value - prev_choice);
                }
                prev_choice = choice_value;
            }
            cout << "downsize przed srednia: " << downsize << "\n";
            downsize /= 5;
            cout << "wspolczynnik obnizenia: " << downsize << "\n";

            result = wybory2[round - 1] - abs(downsize);
        }
    }

    // Upewnienie sie ze zwracany wynik jest w zakresie
    if (result < 180){
        result = 180;
    }
    if (result > 300){
        result = 300;
    }
    return result;
}

int bot_high(int points_bot1, int points_bot2, int round, int wybory1[], int wybory2[])
{
    int result = (rand() % 180) + 180;

    if (result < 180){
        result = 180;
    }
    if (result > 300){
        result = 300;
    }
    return result;
}

